# Coding Style

.DEFAULT_GOAL := help

help:
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
##
## Project setup
##---------------------------------------------------------------------------

fix: ## fix problems
	php-cs-fixer fix src
	vendor/bin/phpstan analyse -c phpstan.neon


##
## database creation
##---------------------------------------------------------------------------
db-reset: ## Drop and recreate database
	./bin/console doctrine:database:drop --force --if-exists
	./bin/console doctrine:database:create

db-migrate: ## make migrations and add fixtures
	./bin/console doctrine:migrations:migrate -n

db-recreate: ## Reset and recreate database with migrations and fixtures
db-dev-create: db-reset db-migrate
	./bin/console doctrine:fixtures:load -n
##
## start and stop server
##---------------------------------------------------------------------------
docker: ## start docker with database
	docker compose up -d

start: ## start symfony server and watch dev
	symfony run -d yarn watch
	symfony serve --no-tls -d
	symfony console doctrine:database:drop --env=dev --force --if-exists
	symfony console doctrine:database:create --env=dev
#	symfony console doctrine:migrations:migrate -n
#	symfony console doctrine:fixtures:load -n --env=dev
	symfony open:local

stop: ## stop server
	symfony server:stop
	docker compose down

##
## testing
##---------------------------------------------------------------------------
db-test-migrate: ## Create test database, loading test data and launch test
	./bin/console doctrine:database:drop --env=test --force --if-exists
	./bin/console doctrine:database:create --env=test
	./bin/console doctrine:migrations:migrate -n --env=test
fixtures: ## Loading test data
	./bin/console doctrine:database:drop --env=test --force --if-exists
	./bin/console doctrine:database:create --env=test
	./bin/console doctrine:migrations:migrate -n --env=test
	./bin/console doctrine:fixtures:load -n --env=test
test: ## Launching PHPUnit tests
	./bin/console doctrine:database:drop --env=test --force --if-exists
	./bin/console doctrine:database:create --env=test
	./bin/console doctrine:migrations:migrate -n --env=test
	./bin/console doctrine:fixtures:load -n --env=test
	./bin/phpunit --testdox

deploy:
	git pull
	composer install --no-dev --optimize-autoloader
	yarn install
	yarn build
	symfony console c:c