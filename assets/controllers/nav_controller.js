import {Controller} from '@hotwired/stimulus';

export default class extends Controller {
    static targets = ['menu', 'wrapper']

    open() {
        if (this.menuTarget.classList.contains("visible")) {
            this.menuTarget.classList.remove("visible");
            this.wrapperTarget.classList.remove("open");
            return
        }
        this.menuTarget.classList.add("visible");
        this.wrapperTarget.classList.add("open");
    }
}
