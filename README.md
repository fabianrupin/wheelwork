# [WheelWork](https://gitlab.com/fabianrupin/wheelwork)

[WheelWork](https://gitlab.com/fabianrupin/wheelwork) is a showcase site describing the activity of the WheelWork company 
[WheelWork](https://wheelwork.io).

## Components and Versions
This project was created on April 2021, and now contains the following components: 
* Symfony 5.2.7, installed with website-full dependencies
* other Symfony components :
  * Symfony Mailer
* Twig/inky-extra, for sending emails
* PhpStan 

## Install the project in development environment

To begin using this project, 

* Clone the repo: `git clone git@gitlab.com:fabianrupin/wheelwork.git`
* Install back dependencies : `composer install`
  
* Install front dependencies and build front files : 
  * `yarn install`
  
* Copy `.env` to `.env.local` file and change `APP_ENV` variable value to `dev
  
* Copy `.env.dev` to `.env.dev.local` file and change variables values to suit your configuration :
  * BDD credentials
  * Admin email
  
* Create and migrate the bdd : 
  * `php bin/console doctrine:database:create`
  * `php bin/console doctrine:migrations:migrate`
  * `php bin/console doctrine:fixtures:load`
  
* Clear the cache : `php bin/console c:c`
  
* Launch the local application : 
  * Launch front files modification watcher : `symfony run -d yarn encore dev --watch`
  * Launch web server in http : `symfony serve --no-tls -d`
  * Launch default browser : `symfony open:local`

## Deploy the app in production environment

* install back dependencies : `composer install --no-dev --optimize-autoloader`
  
* install front dependencies and build front files :
  * `yarn install --force`
  * `yarn build`
  
* Deploy Secrets to production
  * Update or adding secrets production values with `secrets set`
  * Rotate production decryption key with `secrets:generate-keys --rotate`
  * Copy the production decryption key `config/secrets/prod/prod.decrypt.private.php` to your server.
  
* create and migrate the bdd :
  * `php bin/console doctrine:database:create`
  * `php bin/console doctrine:migrations:migrate`
  
* Configure apache configuration
  * In `public` folder copy `.htaccess.dist` to `.htaccess`
  * Comment `Require valid-user` line to remove access with password
  
* clear the cache : `php bin/console c:c`

## Bugs and Issues

Have a bug or an issue with this project? 
[Open a new issue](https://gitlab.com/fabianrupin/wheelwork/-/issues/new) here on gitlab.

## Creator

WheelWork was created by and is maintained by 
**[Fabian Rupin](https://wheelwork.io/)**, Owner of [WheelWork](https://wheelwork.io/).

## Copyright and License

https://readme.so/fr/editor