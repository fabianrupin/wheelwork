<?php

namespace App\Model\DTO;

use Symfony\Component\Validator\Constraints as Assert;

class ContactFormDTO
{
    /**
     * @Assert\Length(
     *     min = 2,
     *     max = 30,
     *     minMessage="contact.firstname.too_short",
     *     maxMessage="contact.firstname.too_long",
     * )
     */
    private string $firstname = '';

    /**
     * @Assert\Length(
     *     min = 2,
     *     max = 30,
     *     minMessage="contact.lastname.too_short",
     *     maxMessage="contact.lastname.too_long",
     * )
     */
    private string $lastname = '';

    /**
     * @Assert\NotBlank(message="contact.email.required")
     * @Assert\Email(message="contact.email.invalid")
     */
    private ?string $email = '';

    /**
     * @Assert\NotBlank(message="contact.message.required")
     * @Assert\Length(
     *     min = 2,
     *     max = 500,
     *     minMessage="contact.message.too_short",
     *     maxMessage="contact.message.too_long",
     * )
     */
    private ?string $message = '';

    public function getFirstname(): string
    {
        return $this->firstname;
    }

    public function setFirstname(?string $firstname): void
    {
        if (is_null($firstname)) {
            $this->firstname = '';
            return;
        }
        $this->firstname = $firstname;
    }

    public function getLastname(): string
    {
        return $this->lastname;
    }

    public function setLastname(?string $lastname): void
    {
        if (is_null($lastname)) {
            $this->lastname = '';
            return;
        }
        $this->lastname = $lastname;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(?string $message): void
    {
        $this->message = $message;
    }

    public function getName(): string
    {
        return ucfirst($this->firstname) . ' ' . ucfirst($this->lastname);
    }
}
