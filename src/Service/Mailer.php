<?php

namespace App\Service;

use App\Model\DTO\ContactFormDTO;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Contracts\Translation\TranslatorInterface;

class Mailer
{
    private MailerInterface $mailer;
    private TranslatorInterface $translator;
    private string $emailAdmin;
    private string $emailNameAdmin;
    private LoggerInterface $logger;

    public function __construct(
        MailerInterface $mailer,
        TranslatorInterface $translator,
        string $emailAdmin,
        string $emailNameAdmin,
        LoggerInterface $logger
    ) {
        $this->mailer = $mailer;
        $this->translator = $translator;
        $this->emailAdmin = $emailAdmin;
        $this->emailNameAdmin = $emailNameAdmin;
        $this->logger = $logger;
    }

    public function sendContactMessage(contactFormDTO $contactFormDTO): TemplatedEmail
    {
        $email = (new TemplatedEmail())
            ->to(new Address($this->emailAdmin, $this->emailNameAdmin))
            ->subject(
                $this->translator->trans(
                    'email.admin.subject',
                    ['name' => $contactFormDTO->getName()],
                    'contact'
                )
            )
            ->htmlTemplate('email/contact.html.twig')
            ->context(['contactFormDTO' => $contactFormDTO, 'emailTo' => $this->emailAdmin]);
        try {
            $this->mailer->send($email);
        } catch (TransportExceptionInterface $e) {
            $this->logger->critical($e->getMessage());
        }

        return $email;
    }
}
