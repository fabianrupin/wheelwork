<?php

namespace App\Form;

use App\Model\DTO\ContactFormDTO;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('firstname', TextType::class, [
                'label' => 'form.firstname.label',
            ])
            ->add('lastname', TextType::class, [
                'label' => 'form.lastname.label',
            ])
            ->add('email', EmailType::class, [
                'label' => 'form.email.label',
            ])
            ->add('message', TextareaType::class, [
                'label' => 'form.message.label',
                'attr' => ['rows' => 10],
            ])
            ->add('captcha', CaptchaType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'translation_domain' => 'contact',
            'data_class' => ContactFormDTO::class,
            'attr' => [
                'novalidate' => 'novalidate',
                'class' => 'form contact__form',
            ],
        ]);
    }
}
