<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LegalNoticesController extends AbstractController
{
    #[Route('/mentions-legales', name: 'legal_notices')]
    public function index(): Response
    {
        return $this->render('legal_notices/index.html.twig');
    }
}
