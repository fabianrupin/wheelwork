<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AboutMeController extends AbstractController
{
    private const BIRTHDAY = '1985-10-06';

    #[Route('/about-me', name: 'about_me')]
    public function index(): Response
    {
        $birthday = new \DateTime(self::BIRTHDAY);

        return $this->render('about_me/index.html.twig', ['birthday' => $birthday]);
    }
}
